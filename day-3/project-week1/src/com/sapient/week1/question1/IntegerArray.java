package com.sapient.week1.question1;

import java.util.Arrays;

import com.sapient.week1.Read;

public class IntegerArray {
	
	private int arr[];
	private int size = 10;
	public int[] getArr() {
		return arr;
	}

	public void setArr(int[] arr) {
		this.arr = arr;
	}
	
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	IntegerArray()
	{
		arr = new int[size];
	}
	
	IntegerArray(int size)
	{
		setSize(size);
		arr = new int[size];
	}
	
	IntegerArray(IntegerArray a)
	{
		setSize(a.getSize());
		setArr(a.getArr());
	}
	
	IntegerArray(int a[])
	{
		setArr(a);
	}
	
	public void sort()
	{
		display();
		Arrays.sort(this.arr);
		display();
	}
	
	public void display() {
	
		System.out.println(Arrays.toString(arr));
	}

	public float average()
	{
		float sum = 0.0f;
		for(int a:arr)
		{
			sum += a;
		}
		return sum/size;
	}
	
	public int search(int no)
	{
		for(int i=0;i<size;i++)
		{
			if(no==arr[i])
				return i;
		}
		return -1;
	}
	
	public void read()
	{
		System.out.println("Enter array: ");
		for(int i=0;i<size;i++)
		{
			arr[0] = Read.sc.nextInt();
		}
	}

}
