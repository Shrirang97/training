package com.sapient.week1.question6;

import com.sapient.week1.Read;

public class View 
{
	public void readData(AddBean ob)
	{
		System.out.println("Enter 2 numbers : ");
		ob.setNum1(Read.sc.nextInt());
		ob.setNum2(Read.sc.nextInt());
	}
	
	public void display(AddBean ob)
	{
		System.out.println(ob.getNum1());
		System.out.println(ob.getNum2());
		System.out.println(ob.getNum3());
	}
	public void finalize()
	{
		System.out.println("View object is destroyed.");
	}

}
