package com.sapient.week1.question9;

import java.util.Comparator;

public class Employee 
{
	private String name;
	private int age;
	
	public Employee(String name, int age)
	{
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	 @Override
	public String toString() {
	        return "\nEmployee [ Name=" + name + ", age=" + age + "]";
	    }
	
}

class NameSorter implements Comparator<Employee>
{
    public int compare(Employee o1, Employee o2)
    {
        return o1.getName().compareTo(o2.getName());
    }
}

class AgeSorter implements Comparator<Employee>
{
   public int compare(Employee o1, Employee o2)
   {
       return o1.getAge() - o2.getAge();
   }
}
