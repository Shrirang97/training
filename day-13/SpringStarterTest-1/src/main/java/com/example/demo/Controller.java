package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller 
{
	@Autowired
	StudentDetailsDAOInterface sDetails;
	@Autowired
	StudentMarksDAOInterface sMarks;
	
	@PostMapping("/addS")
	public String storeStudentDetails(@RequestBody StudentDetails s)
	{
		sDetails.save(s);
		return "Saved Successfully";
	}
	
	@PostMapping("/addM")
	public String storeStudentMarks(@RequestBody StudentMarks s)
	{
		sMarks.save(s);
		return "Saved Successfully";
	}
	@GetMapping("/displayS")
	public List<StudentDetails> displayStudentDetails()
	{
		return (List<StudentDetails>) sDetails.findAll();
	}
	@GetMapping("/displayM")
	public List<StudentMarks> displayStudentMarks()
	{
		return (List<StudentMarks>) sMarks.findAll();
	}
	@GetMapping("/displayS/{id}")
	public Optional<StudentDetails> displayStudentDetailsById(@PathVariable String id)
	{
		return sDetails.findById(id);
	}
	@GetMapping("/displayM/{id}")
	public Optional<StudentMarks> displayStudentMarksById(@PathVariable String id)
	{
		return sMarks.findById(id);
	}
	
}
