package com.example.demo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;



@Entity
public class StudentMarks {
	
//	@ForeignKey(name="regno")
	
	@Id
	private String id;
	@Column
	private String regno;
	@Column
	private int testno;
	@Column
	private int marks1;
	@Column
	private int marks2;
	@Column
	private int marks3;
	
	@Transient
	@OneToMany()
	@JoinColumn(name="regno")
	List<StudentDetails> sref;
	
	public String getRegno() {
		return regno;
	}
	public void setRegno(String regno) {
		this.regno = regno;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getTestno() {
		return testno;
	}
	public void setTestno(int testno) {
		this.testno = testno;
	}
	public int getMarks1() {
		return marks1;
	}
	public void setMarks1(int marks1) {
		this.marks1 = marks1;
	}
	public int getMarks2() {
		return marks2;
	}
	public void setMarks2(int marks2) {
		this.marks2 = marks2;
	}
	public int getMarks3() {
		return marks3;
	}
	public void setMarks3(int marks3) {
		this.marks3 = marks3;
	}
	public StudentMarks() {
		super();
	}
	public StudentMarks(String regno, String id, int testno, int marks1, int marks2, int marks3) {
		super();
		this.regno = regno;
		this.id = id;
		this.testno = testno;
		this.marks1 = marks1;
		this.marks2 = marks2;
		this.marks3 = marks3;
	}
	
	
	

}
