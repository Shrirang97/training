package com.example.demo;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class StudentDAO 
{
	@Autowired
	StudentList l1;
	
	public List<StudentBean> getStudents()
	{
		return l1.getList();
	}
	public List<StudentBean> getDetails(String name)
	{
		return l1.getList().stream().filter(e->e.getName().equals(name)).collect(Collectors.toList());
	}
	
	public List<StudentBean> getDetailsByAgeCity(int age,String city)
	{
		System.out.println(l1.getList().stream().filter(e->e.getAge()==age).filter(e-> e.getCity().equals(city)).collect(Collectors.toList()));
		
		return l1.getList().stream().filter( e-> (e.getAge()==age && e.getCity().equals(city)) ).collect(Collectors.toList());
	}
	public List<StudentBean> getDetailsByAgeRange(int age1, int age2) {
		
		return l1.getList().stream().filter( e-> e.getAge()>=age1 && e.getAge()<=age2 ).collect(Collectors.toList());
	}
	public String insert(StudentBean s)
	{
		l1.getList().add(s);
		return "Added Successfully";
	}
	public String deleteStudent(StudentBean s) {
		
		Iterator it = l1.getList().iterator();
		while(it.hasNext())
		{
//			if((StudentBean)(it.next()))
		}
		return null;
	}
}
