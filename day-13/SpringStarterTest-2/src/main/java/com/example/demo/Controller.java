package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller
{
	@Autowired
	DAO dao;
	@GetMapping("/service1")
	public String getService1()
	{
		
		return dao.template.getForObject(dao.URL_CREATE_STUDENT+"/hello", String.class);
	}

}
