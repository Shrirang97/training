package com.example.demo;
import java.util.Scanner;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

class Read{
    public static Scanner sc = new Scanner(System.in);
}
public class Test3 {

      static final String URL_CREATE_EMPLOYEE = "http://10.151.60.205:8099/student";
      
       public static void main(String[] args) {
    

          Student stud = new Student();
          System.out.println("enter name, age , city");
	          
	     stud.setName(Read.sc.next());
	     stud.setAge(Read.sc.next());
	     stud.setCity(Read.sc.next());
    
          HttpHeaders headers = new HttpHeaders();
          headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
          headers.setContentType(MediaType.APPLICATION_JSON);
    
          RestTemplate restTemplate = new RestTemplate();
    
          // Data attached to the request.
          HttpEntity<Student> requestBody = new HttpEntity<>(stud, headers);
    
          // Send request with POST method.
          Student e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, Student.class);
    
          if (e != null ) {
    
             System.out.println("Employee created: ");
          } else {
             System.out.println("Something error!");
          }
    
       }
}