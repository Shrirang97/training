package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DBConnection.DBconnect;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Connection con = DBconnect.connectDB();
	
	public Login() {
		super();
		
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//get data
		HttpSession session = request.getSession();
		String userid = request.getParameter("username");
        String pwd = request.getParameter("password");
        PrintWriter out = response.getWriter();
        out.print(userid);
        out.print(pwd);
        
        // Connect to mysql and verify username password
        try 
        {
        	
        	PreparedStatement ps = con.prepareStatement("select * from logindetails where userid = ? and password = ?");
        	ps.setString(1, userid);
        	ps.setString(2, pwd);
        	
        	ResultSet rs = ps.executeQuery();
        	out.println(rs);
        	if(rs.next()) 
        	{
        		out.println(rs.getString(1));
            	out.println(rs.getString(2));
            	out.println(rs.getString(3));
        		String s = rs.getString(3);
    			session.setAttribute("userid", userid);
    			
        		if(s.equals("student"))
        		{
        			out.append("student");
        			response.sendRedirect("studenthome.jsp");
        		}
        		else if(s.equals("admin"))
        		{
        			out.append("admin");
        			response.sendRedirect("adminhome.jsp");
        		}
             }
        	else
        	{
        		out.print("Login Failed");
        		
        		response.sendRedirect("Login.jsp");
        	}
        
        } catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e);
        } 
    } 
		
	}
