package DBConnection;
import Models.*;
import java.sql.*;
import java.util.ArrayList;

public class DBconnect 
{
	private static Connection con = null;
	public static Connection connectDB()
	{
		if(con==null)
		{
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","SYSTEM","shadow");
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return con;
	}
	
	public ResultSet getStudentDetails(String userid)
	{
		
	    PreparedStatement ps;
	    ResultSet rs = null;
		try 
		{
			ps = con.prepareStatement("select * from student where userid = ?");
			ps.setString(1, userid);
			rs = ps.executeQuery();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return rs;
	}
	
	public ArrayList<Student> getAllStudents()
	{
		ArrayList<Student> list = new ArrayList<Student>();
		PreparedStatement ps;
	    ResultSet rs = null;
		try 
		{
			ps = con.prepareStatement("select * from student");
			rs = ps.executeQuery();
			while(rs.next())
			{
				String userid = rs.getString(1);
				String name = rs.getString(2);
			    int marks = rs.getInt(3);
			    list.add(new Student(userid, name, marks));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return list;
	}
	
}
