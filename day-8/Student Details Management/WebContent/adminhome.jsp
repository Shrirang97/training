<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="DBConnection.DBconnect,java.sql.*,Models.Student,java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.tabcontent {
  display: none;
  padding: 6px 12px;
  border-top: none;
}
</style>
</head>

<script type="text/javascript">

document.getElementById("Register")[1].style.display = "none";

function switchTab(evt, tabname) {
	  // Declare all variables
	  var i, tabcontent, tablinks;

	  // Get all elements with class="tabcontent" and hide them
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }

	  // Get all elements with class="tablinks" and remove the class "active"
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }

	  // Show the current tab, and add an "active" class to the button that opened the tab
	  document.getElementById(tabname).style.display = "block";
	  evt.currentTarget.className += " active";
	}

</script>

<body>

<center>
<h2>Admin Home</h2>
<div class="tab">
  <button class="tablinks" onclick="switchTab(event, 'List')">Student List</button>
  <button class="tablinks" onclick="switchTab(event, 'Register')">Register Student</button>
</div>
</center>
<br><br>
<div id="List" class="tabcontent">
  <h3>Students List</h3>
  	<%
        DBconnect dbcon = new DBconnect();
    	ArrayList<Student> list = dbcon.getAllStudents();
    	out.println("<table border=\"1\"><tr ><th >userID</th><th >Name</th><th >Marks</th><th >Edit</th><th >Remove</th></tr>");
    	for(Student e : list )
 		{
    		String s = "<tr><th >"+e.getUserid()+"</th><th >"+e.getName()+"</th><th >"+e.getMarks()+"</th><th ><button click=\"edit("+e.getUserid()+","+e.getName()+","+e.getMarks()+")\" >Edit</button></th><th><button onclick=\"delete("+e.getUserid()+","+e.getName()+","+e.getMarks()+")\" >Remove</button></th></tr>"; 
 			out.println(s);
 		}
    	out.print("</table>");
      %>

	
</div>

<div id="Register" class="tabcontent">
  <h3>Register</h3>
	<form action="Register" method="post" >
	 User Id:<br>
	  <input type="text" name="userid" >
	  <br>
	  Password:<br>
	  <input type="password" name="pwd" >
	  <br>
	  Name:<br>
	  <input type="text" name="name" >
	  <br>
	  Percentage:<br>
	  <input type="number" name="marks" >
	  <br><br>
	  <input type="submit" value="Submit">
	</form> 
	
	


</div>



</body>
</html>