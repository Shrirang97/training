package com.sapient.week1.question5;

public class CheckPrime {
	
	public boolean checkPrime(int n)
	{
		if(n<2)
			return false;
		else if(n==2)
			return true;
		
		int sqrt = (int) (Math.sqrt(n)+1);
		
		for(int i=2;i<sqrt;i++)
		{
			if(n%i==0)
				return false;
		}
		
		return true;
	}

}
