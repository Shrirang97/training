package com.sapient.week1.question2;

public class Demo {

	public static void main(String[] args) {
		Matrix m1 = new Matrix();
		Matrix m2 = new Matrix();
		m1.read();
		m2.read();
		//m1.add(m2).display();
		m1.multiply(m2).display();

	}

}
/*
2 2

1 2
4 3

2 2

2 1
1 0

/////////////////

2 3
 
1 2 3
4 5 6
 
2 2

1 2 
3 0

*/