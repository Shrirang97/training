package com.sapient.user.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.exceptions.RollNoNullPointerException;
import com.sapient.student.others.RBundleStudent;
import com.sapient.user.bean.UserBean;
import com.sapient.user.dao.UserDAO;
import com.sapient.user.exception.UserNotFoundException;
import com.sapient.user.exception.UserNullPointerException;
import com.sapient.user.others.RBundleUser;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private ServletContext cnt = null;
	@Override
	public void init(ServletConfig config) throws ServletException {
		
		super.init(config);
		cnt = config.getServletContext();
		/*
		try {
			cnt.setAttribute("userlist", new UserDAO().getAllUsers());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		*/
		
	}

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String userid = request.getParameter("userid");
			if(userid == null) {
				throw new UserNullPointerException(RBundleUser.getValues("ErrorUser0"));
			}
			Map<String,UserBean> list = ((Map<String,UserBean>)cnt.getAttribute("userlist"));
			UserBean ub = list.get(userid);
			
			//System.out.println("List size "+sb+userid);
			
			if(ub == null) {
				throw new UserNotFoundException(RBundleUser.getValues("ErrorUser1"));
			}
			if(ub.getRole().equals("admin"))
			{
				response.sendRedirect("DeleteEdit.jsp");
			}
			else
			{
				response.sendRedirect("marks.jsp");
			}
			
		}
		catch(UserNotFoundException s) {
			
		}
		catch(UserNullPointerException r) {
			
		}
		catch(Exception e) {
			
		}
	}

}
