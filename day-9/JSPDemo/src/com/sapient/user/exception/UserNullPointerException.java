package com.sapient.user.exception;

public class UserNullPointerException extends Exception {

	public UserNullPointerException() {
		// TODO Auto-generated constructor stub
	}

	public UserNullPointerException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UserNullPointerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserNullPointerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserNullPointerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
