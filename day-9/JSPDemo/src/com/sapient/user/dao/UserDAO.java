package com.sapient.user.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Map;
import java.util.TreeMap;

import com.sapient.dbconnetion.oracle.DBConnection;
import com.sapient.student.bean.StudentBean;
import com.sapient.user.bean.UserBean;

public class UserDAO {
public Map<String, UserBean> getAllUsers() throws Exception{
	Connection con = DBConnection.getConnection();
	PreparedStatement ps = con.prepareStatement("Select * from logindetails");
	Map<String,UserBean> map = new TreeMap<String, UserBean>();
	ResultSet rs = ps.executeQuery();
	ResultSetMetaData meta = rs.getMetaData();
	map.put("column_name", new UserBean(meta.getColumnName(1), meta.getColumnName(2),meta.getColumnName(3),0));
	while(rs.next()) {
		String s1 = rs.getString(1);
		String s2 = rs.getString(2);
		String s3 = rs.getString(3);
		int s4 = rs.getInt(4);
		map.put(s1, new UserBean(s1, s2, s3, s4));
	}
	System.out.println("UserDAO---->>>"+map.get("s1"));
	return map;
}
}
