package com.sapient.dataloader;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.admin.dao.AdminDAO;
import com.sapient.student.dao.StudentDAO;
import com.sapient.user.dao.UserDAO;

/**
 * Servlet implementation class DataLoadController
 */
public class DataLoadController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataLoadController() {
        super();
        // TODO Auto-generated constructor stub
    }
    ServletContext cn;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		cn= config.getServletContext();
		try {
			cn.setAttribute("studentlist", new StudentDAO().getAllStudents());
			cn.setAttribute("adminlist", new AdminDAO().getAllAdmins());
			cn.setAttribute("userlist", new UserDAO().getAllUsers());
			System.out.println(cn.getAttribute("studentlist"));
			System.out.println(cn.getAttribute("adminlist"));
			System.out.println(cn.getAttribute("userlist"));
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
