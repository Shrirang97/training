package com.sapient.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Map;
import java.util.TreeMap;

import com.sapient.admin.bean.AdminBean;
import com.sapient.dbconnetion.oracle.DBConnection;
import com.sapient.student.bean.StudentBean;

public class AdminDAO {
public Map<String, AdminBean> getAllAdmins() throws Exception{
	Connection con = DBConnection.getConnection();
	PreparedStatement ps = con.prepareStatement("Select * from admin");
	Map<String,AdminBean> map = new TreeMap<String, AdminBean>();
	ResultSet rs = ps.executeQuery();
	ResultSetMetaData meta = rs.getMetaData();
	map.put("column_name", new AdminBean(meta.getColumnName(1), meta.getColumnName(2)));
	while(rs.next()) {
		String s1 = rs.getString(1);
		String s2 = rs.getString(2);
		//System.out.println(s1+s2);
		map.put(s1, new AdminBean(s1, s2));
	}
	System.out.println("AdminDAO---->>>"+map.get("a1"));
	return map;
}
}
