package com.sapient.admin.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.admin.bean.AdminBean;
import com.sapient.admin.dao.AdminDAO;
import com.sapient.admin.exception.AdminNotFoundException;
import com.sapient.admin.exception.AdminNullPointerException;
import com.sapient.student.bean.StudentBean;
import com.sapient.student.exceptions.RollNoNullPointerException;
import com.sapient.student.exceptions.StudentIDNotFound;
import com.sapient.student.others.RBundleStudent;

/**
 * Servlet implementation class AdminController
 */
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	ServletContext cnt = null;
    public AdminController() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		cnt = config.getServletContext();
		try {
			cnt.setAttribute("adminlist", new AdminDAO().getAllAdmins());
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		/*
		try {
			String userid = request.getParameter("userid");
			if(userid == null) {
				throw new AdminNullPointerException(RBundleStudent.getValues("ErrorAdmin0"));
			}
			Map<String,AdminBean> list = ((Map<String,AdminBean>)cnt.getAttribute("adminlist"));
			AdminBean ab = list.get(userid);
			
			//System.out.println("List size "+sb+userid);
			
			if(ab == null) {
				throw new AdminNotFoundException(RBundleStudent.getValues("ErrorAdmin1"));
			}
			response.sendRedirect("adminhome.jsp");
		}
		catch(AdminNotFoundException s) {
			RequestDispatcher rs = request.getRequestDispatcher("login.jsp");
			request.setAttribute("msg", s.getMessage());
			rs.forward(request, response);
		}
		catch(AdminNullPointerException r) {
			RequestDispatcher rs = request.getRequestDispatcher("login.jsp");
			request.setAttribute("msg", r.getMessage());
			rs.forward(request, response);
		}
		catch(Exception e) {
			
		}
		*/
		
	}

}
