package com.sapient.admin.controller;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.dbconnetion.oracle.DBConnection;


/**
 * Servlet implementation class RegisterStudent
 */
public class RegisterStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PreparedStatement ps = null;
		try {
			ps = DBConnection.getConnection().prepareStatement("Insert into logindetails values (?,?,?,?)");
		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String userid = request.getParameter("userid");
		String name = request.getParameter("name");
		String pwd = request.getParameter("pwd");
		int marks = Integer.parseInt(request.getParameter("marks"));
    	
    	try {
    		ps.setString(1, userid);
        	ps.setString(2, pwd);
			ps.setString(3, "student");
			ps.setInt(4, 0);
			
			if(ps.executeUpdate()>0)
			{	
				ps = DBConnection.getConnection().prepareStatement("commit");
				ps.executeQuery();
				System.out.println("login entered");
				
				ps = DBConnection.getConnection().prepareStatement("Insert into student values (?,?,?)");
				ps.setString(1, userid);
	        	ps.setString(2, name);
				ps.setInt(3, marks);
				if(ps.executeUpdate()>0)
				{
					ps = DBConnection.getConnection().prepareStatement("commit");
					ps.executeQuery();

					System.out.println("student entered");
					response.getWriter().print("<center>Student Registered Successfully<br><br><a href=\"adminhome.jsp\">Home</a></center>");
				}
				else 
				{
					response.getWriter().print("Error");
					System.out.println("Error");
				}
			} else {
				response.getWriter().print("Error 2");
				System.out.println("Error 2");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	
	}

}
