package com.sapient.student.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.StudentBean;
import com.sapient.student.dao.StudentDAO;
import com.sapient.student.exceptions.RollNoNullPointerException;
import com.sapient.student.exceptions.StudentIDNotFound;
import com.sapient.student.others.RBundleStudent;

/**
 * Servlet implementation class StudentMarksController
 */
public class StudentMarksController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentMarksController() {
        super();
        // TODO Auto-generated constructor stub
    }
    ServletContext cn;
    @Override
    public void init(ServletConfig config) throws ServletException {
    	// TODO Auto-generated method stub
    	cn= config.getServletContext();
    	System.out.println("Context: "+cn);
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		try {
			String userid = request.getParameter("userid").trim();
			if(userid == null) {
				throw new RollNoNullPointerException(RBundleStudent.getValues("ErrorStudent0"));
			}
			Map<String,StudentBean> list = ((Map<String,StudentBean>)cn.getAttribute("studentlist"));
			StudentBean sb = (StudentBean)list.get(userid);
			
			System.out.println(userid);
			System.out.println("col : "+list.get(userid));
			System.out.println("List : "+sb+" "+userid+"\n"+list.toString());
			
			if(list.get(userid) == null) {
				throw new StudentIDNotFound(RBundleStudent.getValues("ErrorStudent1"));
			}
			RequestDispatcher rs = request.getRequestDispatcher("marks.jsp");
			request.setAttribute("student", sb);
			rs.forward(request, response);
		}
		catch(StudentIDNotFound s) {
			RequestDispatcher rs = request.getRequestDispatcher("marks.jsp");
			request.setAttribute("msg", s.getMessage());
			rs.forward(request, response);
		}
		catch(RollNoNullPointerException r) {
			RequestDispatcher rs = request.getRequestDispatcher("marks.jsp");
			request.setAttribute("msg", r.getMessage());
			rs.forward(request, response);
		}
		catch(Exception e) {
			
		}
	}

}
