package com.sapient.student.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.dbconnetion.oracle.DBConnection;
import com.sapient.student.bean.StudentBean;

/**
 * Servlet implementation class StudentEditController
 */
public class StudentEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentEditController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userid = request.getParameter("userid");
		String name = request.getParameter("name");
		String per = request.getParameter("per");
		Map<String,StudentBean> map = (Map<String,StudentBean>) request.getServletContext().getAttribute("studentlist");
		StudentBean db = new StudentBean(userid,name,per);
		map.put(userid,db);
		response.sendRedirect("DeleteEdit.jsp");
	
	}

}
