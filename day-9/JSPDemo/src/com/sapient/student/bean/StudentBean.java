package com.sapient.student.bean;

public class StudentBean {
private String id;
private String name;
private String percent;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

public String getPercent() {
	return percent;
}
public void setPercent(String percent) {
	this.percent = percent;
}
public StudentBean(String id, String name, String percent) {
	super();
	this.id = id;
	this.name = name;
	this.percent = percent;
}
public StudentBean() {
	super();
}
@Override
public String toString() {
	return "StudentBean [id=" + id + ", name=" + name + ", percent=" + percent + "]";
}

}
