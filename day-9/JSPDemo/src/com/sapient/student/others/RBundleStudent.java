package com.sapient.student.others;

import java.util.*;

public class RBundleStudent {
public static String getValues(String key) {
	ResourceBundle rs = ResourceBundle.getBundle("student");
	return rs.getString(key);
}
}
