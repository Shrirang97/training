<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.sapient.student.bean.StudentBean,java.util.Map"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<%@include file="logout.jsp" %>
</head>
<%
Map<String,StudentBean> map = (Map<String,StudentBean>) request.getServletContext().getAttribute("studentlist");
//out.print(map.size());
String str = (String)request.getAttribute("status");
if(str==null){ str = " ";}
%>

<body>
<center>
<h1> <%=str%>
Edit Delete Module
</h1>
<table border  = "1">
<%
for(StudentBean sb : map.values())
{
	if(sb.getId().equals("USERID"))
		continue;
%>

<tr>
<td><%=sb.getId() %></td><td><%=sb.getName() %></td><td><%=sb.getPercent() %></td>
<td><a href=Edit.jsp?userid=<%=sb.getId()%>&name=<%=sb.getName()%>&per=<%=sb.getPercent()%>>Edit</a></td>
<td><a href=Delete.jsp?userid=<%=sb.getId()%>&name=<%=sb.getName()%>&per=<%=sb.getPercent()%>>Delete</a></td>
</tr>
<%} %>

</table>
</center>
</body>
</html>