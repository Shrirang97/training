package com.sapient.geolocation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class Geolocation
 */
@Path("/GeoLocation")
public class GeolocationServlet {
	@GET
	@Path("/{param}")
	@Produces(MediaType.TEXT_PLAIN)
	protected void Geolocation(@PathParam("param") String address) throws Exception {
	System.out.println(address);
	System.out.println(address);
	String s = address.replace(" ", "+");
	String basePath = "http://www.mapquestapi.com/geocoding/v1/address?outFormat=json&key=XSN9QJpp94p55HNljAIWyNt1BGSTQSpP&location=";	
	basePath += s;
	URL url = new URL(basePath);
	HttpURLConnection con = (HttpURLConnection) url.openConnection();
	con.setRequestMethod("GET");

    
    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();
    while ((inputLine = in.readLine()) != null)
     	response.append(inputLine);
	in.close();

	
	System.out.println("JSON : "+response);
	JSONObject json = null;
	try {
		
		
		json = new JSONObject(response.toString());
		json = ((JSONObject) (json.getJSONArray("results").getJSONObject(0)
				.getJSONArray("locations").getJSONObject(0)
				.get("latLng") ));
		
		System.out.println(json.get("lat"));
		System.out.println(json.get("lng"));
	} catch (Exception e) {
		// TODO: handle exception
	}
	}
}
