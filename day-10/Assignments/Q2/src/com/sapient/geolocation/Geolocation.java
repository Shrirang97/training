package com.sapient.geolocation;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;


@Path("/Geolocation")
public class Geolocation {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getLocation() throws IOException
	{
		String s = "india";
		String basePath = "http://www.mapquestapi.com/geocoding/v1/address?outFormat=json&key=XSN9QJpp94p55HNljAIWyNt1BGSTQSpP&location=";	
		System.out.println(s);
		basePath += s;
		URL url = new URL(basePath);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
	    int responseCode = con.getResponseCode();
	    //System.out.println("\nSending 'GET' request to URL : " + url);
	    //System.out.println("Response JSON : " + responseCode);
	    
	    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	    String inputLine;
	    StringBuffer response = new StringBuffer();
	    while ((inputLine = in.readLine()) != null)
	     	response.append(inputLine);
		in.close();
		JSONObject json = null;
		try {
		
			System.out.println("JSON : "+response);
			
			json = new JSONObject(response.toString());
			json = ((JSONObject) (json.getJSONArray("results").getJSONObject(0)
					.getJSONArray("locations").getJSONObject(0)
					.get("latLng") ));
			
			System.out.println(json.get("lat"));
			System.out.println(json.get("lng"));
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return json.toString();
	}
}