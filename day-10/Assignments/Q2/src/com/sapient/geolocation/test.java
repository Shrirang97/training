package com.sapient.geolocation;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class test {

	private static URI getBaseUri() {
        return UriBuilder.fromUri("http://192.168.0.5:8081/question_2/").build();
                }
    public static void main(String[] args) {
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(getBaseUri());
        System.out.println(target.path("rest").path("Geolocation").request().accept(MediaType.APPLICATION_JSON).get(String.class));
        
        //System.out.println(target.path("rest").path("hello").request().accept(MediaType.TEXT_PLAIN).get(String.class));
    }
    
    

}
