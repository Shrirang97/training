package com.sapient.geolocation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class testMain {

	public static void main(String[] args) throws IOException, JSONException {
		String address = "984,+Block+F,+Carterpuri+Village,+Sector+23A,+Gurugram,+Haryana+122022,+India";
		String s = address.replace(" ", "+");
		String basePath = "http://www.mapquestapi.com/geocoding/v1/address?outFormat=json&key=XSN9QJpp94p55HNljAIWyNt1BGSTQSpP&location=";	
		System.out.println(s);
		basePath += s;
		URL url = new URL(basePath);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
	    int responseCode = con.getResponseCode();
	    //System.out.println("\nSending 'GET' request to URL : " + url);
	    //System.out.println("Response JSON : " + responseCode);
	    
	    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	    String inputLine;
	    StringBuffer response = new StringBuffer();
	    while ((inputLine = in.readLine()) != null)
	     	response.append(inputLine);
		in.close();

		
		System.out.println("JSON : "+response);
		JSONObject json = null;
		json = new JSONObject(response.toString());
		json = ((JSONObject) (json.getJSONArray("results").getJSONObject(0)
				.getJSONArray("locations").getJSONObject(0)
				.get("latLng") ));
		
		System.out.println(json.get("lat"));
		System.out.println(json.get("lng"));
		
		
	}

}
