package com.sapient.geolocation;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class ClientTest {
	private static URI getBaseURI() {
		 return  UriBuilder.fromUri("http://localhost:8081/question_2").build();
		}
	
	public String useAPI(String address) throws Exception {
	
	 ClientConfig config = new ClientConfig();
    Client client = ClientBuilder.newClient(config);
    WebTarget target = client.target(getBaseURI());
    System.out.println(target.path("rest").path("GeoLocation").path(address).request().accept(MediaType.APPLICATION_JSON).get(String.class));
	return address;

}
}
