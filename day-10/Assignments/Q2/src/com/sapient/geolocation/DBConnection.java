package com.sapient.geolocation;

import java.sql.*;

public class DBConnection {
static Connection con = null;
	
	public static Connection getConnection() throws Exception {
		try {
			if(con==null) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","shadow"); 
			con.setAutoCommit(true);
		} 
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
		
	}

}
