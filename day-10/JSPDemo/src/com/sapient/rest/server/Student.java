package com.sapient.rest.server;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class Student 
{
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getName()
	{
		return "*** WELCOME TO THE REST API ***";
	}
	
	
}
