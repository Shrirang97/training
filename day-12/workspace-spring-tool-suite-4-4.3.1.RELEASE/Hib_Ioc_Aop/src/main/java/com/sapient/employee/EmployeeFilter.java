package com.sapient.employee;

public interface EmployeeFilter 
{
	public void checkForRetirement(EmployeeBean e);
}
