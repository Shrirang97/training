package com.sapient.employee;

public class EmployeeBean {
	
	private String name;
	private int id,age,salary;
	public EmployeeBean() {
		super();
	}
	public EmployeeBean(String name, int id, int age, int salary) {
		super();
		this.name = name;
		this.id = id;
		this.age = age;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "EmployeeBean [name=" + name + ", id=" + id + ", age=" + age + ", salary=" + salary + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	
	
	

}
