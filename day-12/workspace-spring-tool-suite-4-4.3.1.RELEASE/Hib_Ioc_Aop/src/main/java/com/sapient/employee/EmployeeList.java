package com.sapient.employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeList 
{
	ArrayList<EmployeeBean> list = new ArrayList<EmployeeBean>();

	public EmployeeList(List<EmployeeBean> list) {
		super();
		this.list = (ArrayList)list;
	}

	public EmployeeList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArrayList<EmployeeBean> getList() {
		return list;
	}

	public void setList(ArrayList<EmployeeBean> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "EmployeeList [list=" + list + "]";
	}
	
	
}
