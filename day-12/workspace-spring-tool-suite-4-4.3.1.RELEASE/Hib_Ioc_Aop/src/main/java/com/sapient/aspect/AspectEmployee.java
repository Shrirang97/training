package com.sapient.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class AspectEmployee
{
	@Before(value="execution(* *.*(EmploeeBean))")
	public void checkForRetirement(JoinPoint jpoint)
	{
		
	}
	
}
