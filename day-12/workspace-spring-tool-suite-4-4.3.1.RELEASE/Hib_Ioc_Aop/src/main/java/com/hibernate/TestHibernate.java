package com.hibernate;

import java.util.ArrayList;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.sapient.employee.EmployeeBean;
import com.sapient.employee.EmployeeList;
import com.sapient.employee.JavaContainer;


public class TestHibernate {

	public static void main(String[] args)
	{
		
	    ApplicationContext context = new  AnnotationConfigApplicationContext(JavaContainer.class);
	    
	    EmployeeList emplist = context.getBean(EmployeeList.class);
	    
	    ArrayList<EmployeeBean> list = emplist.getList();
	    
	    
	    
		System.out.println(context);
		
		
	}

}
