package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArithmeticAspects {
	
	@Before(value = "execution( * *.*(double,double))")
	public void check1(JoinPoint jpoint)
	{
		for(Object x: jpoint.getArgs())
		{
			double a = (Double) (x);
			if(a<0)
			{
				throw new IllegalArgumentException("Number cannot be negative");
			}
		}
	}

}
