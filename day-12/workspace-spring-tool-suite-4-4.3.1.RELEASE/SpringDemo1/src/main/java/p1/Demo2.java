package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo2 {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaContainer.class);
		Hello obj;
		obj = context.getBean(Hello.class);
		obj.display();
		
		Employee emp = context.getBean(Employee.class);
		System.out.println(emp);
		
		ListOfHolidays list;
		list = context.getBean(ListOfHolidays.class);
		System.out.println(list);
	}

}
