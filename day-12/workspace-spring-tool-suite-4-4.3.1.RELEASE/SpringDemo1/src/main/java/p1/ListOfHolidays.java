package p1;

import java.util.*;

public class ListOfHolidays 
{
	private List<Holiday> holidays = new ArrayList<Holiday>();

	public ListOfHolidays(List<Holiday> holidays) {
		super();
		this.holidays = holidays;
	}

	public ListOfHolidays() {
		super();
		// TODO Auto-generated constructor stub
	}

	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((holidays == null) ? 0 : holidays.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListOfHolidays other = (ListOfHolidays) obj;
		if (holidays == null) {
			if (other.holidays != null)
				return false;
		} else if (!holidays.equals(other.holidays))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ListOfHolidays [holidays=" + holidays + "]";
	}

	public List<Holiday> getHolidays() {
		return holidays;
	}

	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}
	
	
	
}
