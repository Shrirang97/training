package p1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {
	@Bean
	@Scope("singleton")
	public Hello getHello()
	{
		return new Hello();
	}
	
	@Bean
	@Scope("singleton")
	public Employee getEmp()
	{
		return new Employee("Shrirang",21,"Mumbai");
	}
	
	@Bean
	@Scope("singleton")
	public ListOfHolidays getListOfHolidays()
	{
		ListOfHolidays list = new ListOfHolidays();
		list.getHolidays().add(new Holiday("01/26/2019","Republic Day"));
		return list ;
	}

}
