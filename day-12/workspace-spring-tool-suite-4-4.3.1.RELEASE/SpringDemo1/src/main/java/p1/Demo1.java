package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo1 
{

	public static void main(String[] args) 
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		Holiday ob;
		ob = (Holiday)context.getBean("h1");
		System.out.println(ob.toString());
		ob = (Holiday)context.getBean("h2");
		System.out.println(ob.toString());
		ob = (Holiday)context.getBean("h3");
		System.out.println(ob.toString());
		
		ListOfHolidays obj = (ListOfHolidays) context.getBean("hlist");
		System.out.println(obj);
		
		MapOfHolidays obm = (MapOfHolidays) context.getBean("holidaymap");
		System.out.println(obm);
		
		Employee emp = (Employee) context.getBean("emp");
		System.out.println(emp);
	}

}
