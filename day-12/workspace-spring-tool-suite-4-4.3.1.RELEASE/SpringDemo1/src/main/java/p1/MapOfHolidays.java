package p1;

import java.util.*;

public class MapOfHolidays 
{
	Map<String,Holiday> map = new HashMap<String, Holiday>();

	public MapOfHolidays(Map<String, Holiday> map) {
		super();
		this.map = map;
	}

	public MapOfHolidays() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "MapOfHolidays [map=" + map + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MapOfHolidays other = (MapOfHolidays) obj;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		return true;
	}

	public Map<String, Holiday> getMap() {
		return map;
	}

	public void setMap(Map<String, Holiday> map) {
		this.map = map;
	}
	
	
}
