package com.sapient.restaurant.controller;
import java.net.URI;
import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.sapient.restaurant.bean.RestaurantBean;

public class RestaurantDAO {

	public static ArrayList<RestaurantBean> getRestaurants()
	{
		ArrayList<RestaurantBean> list = new ArrayList<RestaurantBean>();

		RestaurantBean e = new RestaurantBean("G1", "Gurgram", 5, new int[5]);
		list.add(e);
		e = new RestaurantBean("D1", "Delhi", 5, new int[5]);
		list.add(e);
		e = new RestaurantBean("G2", "Gurgram", 6, new int[6]);
		list.add(e);
		e = new RestaurantBean("D2", "Delhi", 6, new int[6]);
		list.add(e);
		return list;
	}
	
	private static URI getBaseUri() {
        return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/cities?q=delhi").build();
                }
	
	public static ArrayList<RestaurantBean> getZOMATORestaurants()
	{
		ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(getBaseUri());
        target.request().header("user-key", "552e82e302b74c48ae53c995b965494e");
        System.out.println(((Builder) target).accept(MediaType.TEXT_PLAIN).get(String.class));
        
        ArrayList<RestaurantBean> list = new ArrayList<RestaurantBean>();

        RestaurantBean e = new RestaurantBean("G1", "Gurgram", 5, new int[5]);
		//list.add(e);
		
		return list;
		
	}

}
