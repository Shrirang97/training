package com.sapient.restaurant.bean;

import java.util.Arrays;

public class RestaurantBean {

	private String name;
	private String location;
	private int tablesnos;
	private int table[];
	public RestaurantBean(String name, String location, int tables, int[] table) {
		super();
		this.name = name;
		this.location = location;
		this.tablesnos = tables;
		this.table = table;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Arrays.hashCode(table);
		result = prime * result + tablesnos;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RestaurantBean other = (RestaurantBean) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (!Arrays.equals(table, other.table))
			return false;
		if (tablesnos != other.tablesnos)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "RestaurantBean [name=" + name + ", location=" + location + ", tables=" + tablesnos + ", table="
				+ Arrays.toString(table) + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public int getTablesnos() {
		return tablesnos;
	}
	public void setTablesnos(int tablesnos) {
		this.tablesnos = tablesnos;
	}
	public int[] getTable() {
		return table;
	}
	public void setTable(int[] table) {
		this.table = table;
	}
	

}
