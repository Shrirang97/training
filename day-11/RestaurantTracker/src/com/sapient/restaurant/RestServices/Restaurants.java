package com.sapient.restaurant.RestServices;

import java.util.ArrayList;
import java.util.Arrays;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import com.sapient.restaurant.bean.RestaurantBean;
import com.sapient.restaurant.controller.RestaurantDAO;

@Path("/restaurants")
public class Restaurants {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getRestaurants()
	{
		ArrayList<RestaurantBean> list = RestaurantDAO.getRestaurants();
		String s = "";
		for(RestaurantBean e: list)
		{
			s += e.toString()+"\n";
		}
		return s;
	}
	@GET
	@Path("/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getTables(@PathParam("name") String name)
	{
		ArrayList<RestaurantBean> list = RestaurantDAO.getRestaurants();
		String s = "";
		for(RestaurantBean e: list)
		{
			if(e.getName().equals(name))
			{
				s = Arrays.toString(e.getTable());
				break;
			}
		}
		return s;
	}
	
	@GET
	@Path("/{name}/1/{tableno}")
	@Produces(MediaType.TEXT_PLAIN)
	public String bookTable(@PathParam("name") String name, @PathParam("tableno") int tableno)
	{
		ArrayList<RestaurantBean> list = RestaurantDAO.getRestaurants();
		String s = "";
		for(RestaurantBean e: list)
		{
			if(e.getName().equals(name))
			{
				s = Arrays.toString(e.getTable()) +"\nTable Booked\n";
				int a[] = e.getTable();
				if(a[tableno]==0)
				{
					a[tableno] = 1;
					s = Arrays.toString(e.getTable());
				}
				else
				{
					s += "is already booked.";
				}
			}
		}
		return s;
	}

}
