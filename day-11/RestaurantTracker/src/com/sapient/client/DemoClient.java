package com.sapient.client;

import java.net.URI;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONObject;
import javax.json.*;

public class DemoClient {


	private static URI getBaseUri(String s) {
        return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/cities?q="+s).build();
                }
	
	public static void main(String args[]) throws Exception
	{
		String city = "delhi";
		ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(getBaseUri(city));
        
        String response = target.request().header("user-key", "552e82e302b74c48ae53c995b965494e").accept(MediaType.TEXT_PLAIN).get(String.class);
        System.out.println(response);

        
        JSONObject json = new JSONObject(response);
        		
		System.out.println("JSON String\n"+json.toString());
        //RestaurantBean e = new RestaurantBean("G1", "Gurgram", 5, new int[5]);
		
		
	}
    
    
    
}