package com.sapient.testmain;

import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.boot.Metadata;  
import org.hibernate.boot.MetadataSources;  
import org.hibernate.boot.registry.StandardServiceRegistry;  
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.sapient.employee.EmployeeBean;  

public class testmain {

	public static void main(String[] args)
	{
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		
		Session session = factory.openSession();
		
		Transaction t = session.beginTransaction();
		
		EmployeeBean e = new EmployeeBean(1,"Shrirang","Pinjarkar");
		
		session.save(e);
		
	
		e = new EmployeeBean(2,"Sachin","Rana");

		session.save(e);
		
		e = session.get(EmployeeBean.class, Integer.valueOf(2));
		e.setLastName("Demo");
		System.out.println(e);
		
		EmployeeBean e1 = session.load(EmployeeBean.class, Integer.valueOf(2));
		System.out.println(e1);

		
		EmployeeBean e2 = session.get(EmployeeBean.class, Integer.valueOf(2));
		System.out.println(e2);
		t.commit();
		
		//session.per
		
		System.out.println("Successfully Saved");
		
		factory.close();
		
		session.close();
	}

}
